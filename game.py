from os import sep
from random import randint
def game_introduction():
  print("Let's play bulls and cows!\n--------------------------\nI have a secret four numbers.\nCan you guess it?\nYou tell me how many tries you want!")

def generate_secret_number():
  secret_number = []
  while len(secret_number) < 4:
    new_rand_int = randint(0,9)
    if new_rand_int not in secret_number:
      secret_number.append(new_rand_int)
  return secret_number

def guess_to_list_of_integers(guess):
  guess_list = []
  for string in guess:
    current_num = int(string)
    guess_list.append(current_num)
  return guess_list

def guess(guess_number):
  guess_number = str(guess_number)
  guess = input('Type in guess #' + guess_number + ': ')
  while len(guess) > 4:
    print("You must enter a four-digit number")
    guess = input('Type in guess #' + guess_number + ': ')
  if guess == 'exit':
    print('Exiting the game! Bye!')
    exit()
  guess = guess_to_list_of_integers(guess)
  return guess

def exact_matches(secret_number, guess):
  pairs = zip(secret_number, guess)
  exact_matches = 0
  for pair in pairs:
    if pair[0] == pair[1]:
      exact_matches += 1
  return exact_matches

def common_matches(secret_number, guess):
  common_matches = 0
  for num in secret_number:
    if num in guess:
      common_matches += 1
  return common_matches

def score(secret_number, guess):
  bulls = exact_matches(secret_number, guess)
  cows = common_matches(secret_number, guess)
  score_card = {"bulls": bulls, "cows": cows}
  return score_card

def print_score_pretty(score_card):
  for key in score_card:
    print('You have ', score_card[key],' ', key, '!', sep='')

def is_winner(secret_number, guess):
  return secret_number == guess

def play_game():
  game_introduction()
  secret_number = generate_secret_number()
  number_of_guesses = 1
  score_card = {'bulls': 0, 'cows': 0}
  how_many_guesses = input("How many guesses would you like?")

  while number_of_guesses <= int(how_many_guesses):
    current_guess = guess(number_of_guesses)
    if is_winner(secret_number, current_guess):
      print('You guessed my secret!')
      repeat_game = input('Do you want to play again? (y/n)')
      if repeat_game == 'n':
        exit()
      else:
        play_game()
    number_of_guesses += 1
    current_score = score(secret_number, current_guess)
    for item in current_score:
      for n in score_card:
        if item == n:
          score_card[n] += current_score[item]
    print_score_pretty(score_card)

  print("I'm sorry. You didn't guess my number!")
  print("My secret number was", secret_number)
  repeat_game = input('Do you want to play again? (y/n)')
  if repeat_game == 'n':
    exit()
  else:
    play_game()

play_game()